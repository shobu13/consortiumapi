import graphene
from graphene import relay

import core.schema


class Query(core.schema.Query, graphene.ObjectType):
    pass


class Mutations(core.schema.Mutations, graphene.ObjectType):
    pass


schema = graphene.Schema(query=Query, mutation=None)
