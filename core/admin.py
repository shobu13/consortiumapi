from django.contrib import admin
from django.contrib.admin import ModelAdmin

from core.models import *


class FileSystemItemAdmin(ModelAdmin):
    prepopulated_fields = {'name': ('file',)}
    list_display = ('__str__', 'file', 'is_file')
    list_filter = ('folder',)


admin.site.register(FileSystemItem, FileSystemItemAdmin)
