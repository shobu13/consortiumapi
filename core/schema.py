import graphene
from graphene_django import DjangoObjectType
from core.models import FileSystemItem


class FileScalar(graphene.Scalar):
    @staticmethod
    def serialize(file):
        if file:
            return file.url
        return ""


class FileSystemItemType(DjangoObjectType):
    file = FileScalar()
    path = graphene.String()

    class Meta:
        model = FileSystemItem


class Query(object):
    file_system_items = graphene.List(FileSystemItemType)
    file_system_item_by_path = graphene.Field(FileSystemItemType, path=graphene.String(required=True))

    @staticmethod
    def resolve_file_system_items(parent, info, **kwargs):
        return FileSystemItem.objects.select_related('folder').all()

    @staticmethod
    def resolve_file_system_item_by_path(parent, info, path):
        path = [i for i in path.split("/") if i != ""]
        item = FileSystemItem.objects.get(name="/")
        for i in path:
            item = item.items.get(name=i)
        return item


class Mutations(graphene.ObjectType):
    pass
