from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets
from rest_framework import permissions

from core.models import FileSystemItem
from core.serializers import FileSystemItemSerializer


class FileSystemItemViewSet(viewsets.ModelViewSet):
    queryset = FileSystemItem.objects.all()
    serializer_class = FileSystemItemSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]
    filterset_fields = ['name']
