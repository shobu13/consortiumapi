from django.db import models


class FileSystemItem(models.Model):
    name = models.CharField(max_length=500)
    file = models.FileField(blank=True, null=True)
    is_file = models.BooleanField(editable=False)

    folder = models.ForeignKey('core.FileSystemItem', on_delete=models.CASCADE, null=True, related_name='items')

    @property
    def path(self):
        return self.__str__()

    def __str__(self):
        if self.name == '/':
            return '/'
        if self.folder.name == '/':
            return '/' + self.name
        return str(self.folder) + '/' + self.name

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.is_file = bool(self.file)
        super(FileSystemItem, self).save()
