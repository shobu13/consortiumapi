from rest_framework import serializers

from core.models import FileSystemItem


class FileSystemItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = FileSystemItem
        fields = ['id', 'name', 'file', 'folder', 'items', 'is_file', 'path']
        depth = 1
