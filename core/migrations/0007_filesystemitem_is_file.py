# Generated by Django 3.0.2 on 2020-01-23 14:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0006_auto_20200123_1044'),
    ]

    operations = [
        migrations.AddField(
            model_name='filesystemitem',
            name='is_file',
            field=models.BooleanField(default=False),
            preserve_default=False,
        ),
    ]
